# Помощь

[TOC]


# Структура поисковой системы ЭЛЕКТРОННЫЙ КАТАЛОГ

### Каталог
Содержит библиографические записи на все виды документов (книги, журналы, газеты, статьи..) из фонда НТБ, изданные на различных носителях (бумажные, аудио-, видео-, электронные…) в разные хронологические периоды.

Гарантированная полнота с 1986 года.

В каталоге также представлены издания, отсутствующие в фонде библиотеки, авторами которых являются ученые ТПУ.

### Периодические издания
Журналы, реферативные журналы, газеты специально продублированы в отдельный каталог, организованный в алфавитной таблице, для удобства заказа изданий в автоматизированном режиме.
Полноценный тематический поиск по журналам проводится в «Каталоге». В «Периодических изданиях» возможен поиск только по названию. От него можно перейти к номерам, а от номера к статьям, если они представлены, по вкладке «Связанные документы».

Русскоязычные сериальные издания отражены полностью, иностранные журналы планомерно отражаются.
За закладкой Ещё представлены:

### Труды ученых ТПУ
Электронный справочник включает библиографические списки опубликованных работ профессоров и научных сотрудников Томского политехнического университета, внесших значительный вклад в развитие вуза, биографическую информацию и сведения о литературе, посвященной жизни и деятельности ученых.

### Электронная библиотека
Электронные коллекции включают полные тексты монографий, авторефератов, материалов конференций, учебных, справочных и периодических изданий, патентов и других трудов сотрудников ТПУ. Формат выкладываемых произведений: PDF, HTML и др. 

### Авторитетный файл – Имена лиц
Применяется для точной формулировки поискового запроса и идентификации частных лиц. Включает варианты разночтений,  разные формы имени лица (псевдонимы, подлинная фамилия, варианты написания в различных языках, светская фамилия духовных лиц), даты жизни, содержит справочную информацию.
Представленные имена используются в электронных каталогах в качестве автора (соавтора, редактора, составителя, научного руководителя и др.) или персоналии (о нём).

### Авторитетный файл – Организации
Применяется для точной формулировки поискового запроса и идентификации организаций. Включает все прежние наименования организации и существующие варианты наименований, в том числе – аббревиатуры.  Содержит текст, описывающий историю наименований и другие характеристики организации.

Представленные организации используются в электронных каталогах в качестве коллективного автора или организации как предмет (о ней).

### Авторитетный файл – Предметные рубрики
Содержит перечень предметных рубрик, отражающих содержание документов и классификационные индексы УДК, ББК.
Применяется для предметного тематического поиска документов в электронном каталоге.

# Режимы работ
* **Анонимный режим** работы с электронным каталогом, справочником «Труды учёных ТПУ» и авторитетными файлами позволяет осуществлять только поиск библиографических описаний.
* **Авторизованный режим** работы позволяет осуществлять не только поиск, но и просмотр информации о местонахождении и доступности изданий, их заказ, использование функций личного кабинета.

# Вход в систему
Существует несколько способов:

* по идентификатору (номеру читательского билета) и паролю, полученному в Бюро регистрации (ком. 123). Это единственная возможность для внешних пользователей библиотеки.

Для студентов и сотрудников ТПУ кроме этого способа более удобны, могут быть другие:

* вход через портал ТПУ
* вход через социальные сети

Для этого необходимо однократно выполнить **привязку** учетной записи (портального аккаунта и социальных сетей) к идентификатору читателя и паролю, полученному в Бюро регистрации (ком. 123).

![Форма привязки текущей учетной записи к идентификатору читателя библиотеки](http://catalog.lib.tpu.ru/images/op1.jpg "Форма привязки текущей учетной записи к идентификатору читателя библиотеки")

# Поиск в каталоге

### Поиск одной строкой
Позволяет искать документы в свободной форме по любому признаку. При поиске каталог предлагает «всплывающие подсказки».

![Пример ввода запроса в свободной форме без использования всплывающих подсказок](http://catalog.lib.tpu.ru/images/op2_1.jpg "Пример ввода запроса в свободной форме без использования всплывающих подсказок")

Если Вы не хотите ими воспользоваться - надо нажать клавишей на открытое пустое поле страницы. В поисковой строке сохранится Ваш запрос. 

![Поиск одной строкой](http://catalog.lib.tpu.ru/images/op3.jpg "Поиск одной строкой")

###Расширенный поиск
Смысл поискового запроса определяется поисковым атрибутом и ограничением результатов поиска (при необходимости) по виду материала, году издания, фонду, языку текста.
Поисковые атрибуты:


Атрибут | Описание
-------:|----------
**заглавие** | название документа, в том числе серии
**автор** | фамилия автора, редактора, составителя, название организации, учреждения, наименование конференции, съезда, **общественной организации
**тематика** | ключевые слова, персоналии (о ком)
**ISBN** | международный стандартный книжный номер
**ISSN** | международный стандартный серийный номер
**год публикации** | год издания документа
**место публикации** | город публикации
**издательство** |
**везде** | любое слово, число, термин из описания документа
**классификация УДК** | Универсальная десятичная классификация
**классификация ББК** | Библиотечно-библиографическая классификация
**классификация OKSVNK** | Общероссийский классификатор специальностей высшей научной квалификации
**дата занесения записи в БД** | для служебного использования
**идентификатор записи** | для служебного использования
**все записи?** | используется только при отсутствии поискового атрибута в связке с ограничением результата поиска.  


![Форма расширенного поиска](http://catalog.lib.tpu.ru/images/op4.jpg "Форма расширенного поиска")

### Поля запроса
Поиск осуществляется по одному или нескольким словам из любого известного поискового атрибута:

* поиск может производиться по усеченной части слова (слева, справа, слева и справа одновременно)
* прописные и строчные буквы не различаются
* ключевые слова пишутся во множественном числе
* при формировании запроса учитываются правила используемого языка

Применение нескольких поисковых атрибутов дает наиболее точный результат.
Для получения релевантного результата поиска  важно точно указывать позицию слов в поисковом запросе:

Атрибут | Описание
-------:|---------
**точно** | точное совпадение слов с поисковым запросом
**все** | используются все слова в поисковом запросе, порядок слов не имеет значения
**любое** | любое из слов в поисковом запросе

Помните, что просмотр сведений о местонахождении и доступности документа возможен только в **авторизованном режиме** работы.

### Сортировка по релевантности
Результаты поиска сортируются по релевантности, то есть по соответствию запросу.
Имеется возможность отсортировать результаты поиска:
* по дате публикации (в прямой и обратной хронологии)
* по автору (в прямом и обратном алфавите)
* по ключу записей (для служебного использования)

### Фасеты
Позволяют уточнить запрос по автору, тематике, издательству, году публикации и фонду.

### Заказ
Заказ документов возможен из фондов научной, редкой, учебной и художественной литературы.
Подъем из фондов редкой, учебной и художественной литературы производится по требованию пользователей. 
Подъем из фонда научной литературы в 11.00 и 15.00 часов с понедельника по пятницу.

Заказанные документы из соответствующих фондов можно получить:

Фонд | Физическое расположение
-----|------------------------
**Научный фонд** | на абонементе научной литературы (ком. 117), читальном зале технической литературы (ком. 309)
**Редкий фонд** | в отделе редких книг (ком. 315), читальном зале технической литературы (ком. 309)
**Учебный фонд** | на абонементе учебной литературы (ком. 124)
**Художественный фонд ** | на абонементе художественной литературы (ком. 207)

в течение **5 дней** с момента выполнения заказа.

Одновременно можно заказать не более 10 документов. Повтор заказа возможен после выполнения первого.

> ##Внимание!##
>
> Литература для служебного пользования (ДСП) выдается для работы в читальном зале при наличии заявления с
> указанием цели использования издания, подписанного проректором по режиму и безопасности.
> Бланк заявления можно получить по ссылке
> [http://catalog.lib.tpu.ru/misc/dspblank.doc](http://catalog.lib.tpu.ru/misc/dspblank.doc "Бланк заказа").

# Личный кабинет

### Контроль выполнения заказа
Предназначен для удаленного отслеживания выполнения заказа.
Статус заказа может принимать одно из следующих значений:

Статус заказа | Описание
-------------:|---------
**Еще не выполнен** | заказ поступил в систему, но еще не принят к исполнению соответствующей службой.
**Выполнен** | заказ успешно выполнен, и документ находится в точке книговыдачи. В течение 5 дней можно получить документ.
**Документ выдан** | заказ успешно выполнен, и документ выдан читателю.
**Отказ** | выполнение заказа невозможно по какой либо причине, пользователь может получить диагностическое сообщение с объяснением причины отказа.

### Электронный формуляр
Предназначен для удаленного отслеживания полученных документов во всех структурных подразделениях библиотеки и сроков их возврата.

Данный режим работы в любое время позволяет уточнить список взятых документов, сроки и место их возврата, а также информацию о неустойке за нарушение сроков возврата изданий.

### Архив
Позволяет уточнить информацию обо всех ранее взятых Вами документах. 

### Корзина / Избранное
При поиске или заказе имеется возможность сохранить нужное описание в Корзине или Избранном.

Корзина сохраняет информацию только на период работающей сессии.

Избранное позволяет сохранять информацию для последующей работы.