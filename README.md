# README

Список документов для программного обеспечения  `Project_TPU/OPAC`:

* [Руководство системного администратора](https://bitbucket.org/project_tpu/opac-docs/src/master/ag.md)
* [Руководство пользователя](https://bitbucket.org/project_tpu/opac-docs/src/master/ug.md)
* [Помощь](https://bitbucket.org/project_tpu/opac-docs/src/master/help.md)
* [История](https://bitbucket.org/project_tpu/opac-docs/src/master/history.md)
