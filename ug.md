~~** РУКОВОДСТВО ПОЛЬЗОВАТЕЛЯ **~~
* * *
[TOC]
* * *
# Конфигурация из 2 виртуальных машин


## Схема

![Схема клеток](https://bitbucket.org/project_tpu/opac-docs/downloads/Project_TPU_OPAC.png)

## Характеристики виртуальных машин

Характеристика | Виртуальная машина 1 | Виртуальная машина 2
---------------|----------------------|---------------------
ЦП             | 4                    | 4
ОЗУ            | 4Гб                  | 4Гб
ЖД1            | 31Гб                 | 31Гб
ЖД2            | -                    | 500Гб
ОС             | FreeBSD 11.2         | FreeBSD 11.2

## Виртуальная машина 1

Виртуальная машина 1 предназначена для работы веб-приложения и связанных с ним сервисов.

### Подготовка

Подготовка включает в себя развертывание и старт клеток `nginx`, `node`, `pg` и `gearman` из архивных
файлов (см. `/usr/jails/ezjail_archives`). Для развертывания клеток выполним команды -

    sudo ezjail-admin restore nginx
    sudo ezjail-admin restore node
    sudo ezjail-admin restore pg
    sudo ezjail-admin restore gearman

Далее выполним старт клеток -
 
    sudo ezjail-admin start gearman pg node nginx

Текущее состояние клеток всегда можно получить с помощью команды

    ezjail-admin list
    
Если все хорошо, то клетки установлены и находятся в активном состоянии. Подготовка выполнена.

### Создание базы данных

```
План:
-----
1. Создать пользователя opacusr1
2. Создать базу данных opac
3. Разрешить удаленный доступ к базе данных для веб-приложения
4. Выполнить рестарт сервера баз данных
```

Веб-приложение использует сервис базы данных как сервис *долговременной памяти*. База данных создается в
клетке `pg`. Для удаленной работы с базой данных необходимо задать: имя базы данных, имя и пароль пользователя.

Для того что бы создать пользователя `opacusr1` и базу данных `opac` выполним команды -


    sudo ezjail-admin console pg
    pg#su -l pgsql
    $createuser -SdrP opacusr1
    $createdb --owner=opacusr1 opac
    $exit
    pg#exit

Для разрешения удаленного доступа к базе данных, со стороны веб-приложения, добавим в файл `/usr/local/psql/data/pg_hba.conf` строку -  

```
host    all             all             10.0.0.101/32            trust
```

Здесь `10.0.0.101` адрес клетки `node`, в которой работает веб-приложение.

В завершении выполним рестарт сервера -

    sudo ezjail-admin console pg
    pg#service pg restart

После успешного выполнения этих команд база данных `opac` создана, и к ней можно подключиться от
имени пользователя `opacusr1`.


### Развертывание веб-приложения

```
План:
-----
1. Создать конфигурационный файл opac.conf
2. Выполнить установочный сценарий setup.sh
3. Проверить статус веб-приложения 
```

В клетке `node`, от имени пользователя `libuser`, необходимо задать переменные окружения для веб-приложения. Для этого
необходимо выполнить вход в клетку `node` -

    ssh libuser@10.0.0.101


Эти переменные указываются в файле `~/opac/opac.conf` и переопределяют ранее заданные значения по умолчанию в
файле `~/opac/defaults/opac.conf`. Дополним конфигурацию веб-приложения данными о параметрах подключения к
базе данных -

    node$cd ~/opac
    node$echo 'DATABASE_URL="postgresql://<USERNAME>:<PASSWORD>@10.0.0.102/opac"' >> opac.conf

После того как заданы переменные окружения необходимо выполнить сценарий `setup.sh` -

    node$sh ./setup.sh
    
В результате выполнения этого сценария будет установлено/развернуто веб-приложение, с заданными
переменными окружения.   

Для проверки статуса веб-приложения введите команду -

    node$pm2 status
        
Веб-приложение развернуто и работает под управлением менеджера процессов - программы [pm2](https://pm2.io/runtime/).

Веб-приложение в дальнейшем будет автоматически запускаться и останавливаться, синхронно с клеткой.

### Внесение изменений в конфигурацию веб-приложения

```
План:
-----
1. Остановить работу веб-приложения
2. Удалить текущую конфигурацию
3. Создать новый или изменить прежний конфигурационный файл opac.conf
4. Выполнить установочный сценарий setup.sh
5. Проверить статус веб-приложения 
```

Веб-приложение работает под управлением менеджера процессов (программа pm2) и для внесения изменений в конфигурацию
необходимо выполнить операцию противоположную развертыванию, а затем снова выполнить развертывание, но уже с новой
конфигурацией.

Для остановки веб-приложения и удаления текущей конфигурации выполняем команды -

    node$pm2 stop opac
    node$pm2 delete opac

Вносим изменения в переменные окружения в файле `opac.conf` -

    node$echo 'RUSLAN_HOST="ruslan.lib.tpu.ru"' >> opac.conf
    node$echo 'RUSLAN_PORT="2100"' >> opac.conf

После того как заданы переменные окружения необходимо выполнить сценарий `setup.sh` -

    node$sh ./setup.sh

Для проверки статуса веб-приложения -

    node$pm2 status

Веб-приложение снова развернуто и работает с новой конфигурацией.

## Виртуальная машина 2

Виртуальная машина 2 предназначена для работы с поисковым индексом.

### Подготовка

Подготовка включает в себя развертывание и старт клеток из архивных файлов (см. `/usr/jails/ezjail_archives`). Для развертывания клеток `zebrahub`, `zebraslave1` и `zebraslave2` выполним команды -

```
#!sh
sudo ezjail-admin create -r /mnt/zebrahub -a /usr/jails/ezjail_archives/zebrahub-<timestamp>.tar.gz zebrahub 10.0.0.104
sudo ezjail-admin create -r /mnt/zebraslave1 -a /usr/jails/ezjail_archives/zebraslave-<timestamp>.tar.gz zebraslave1 10.0.0.105
sudo ezjail-admin create -r /mnt/zebraslave2 -a /usr/jails/ezjail_archives/zebraslave-<timestamp>.tar.gz zebraslave2 10.0.0.106
```

*Здесь клетки `zebraslave1` и `zebraslave2` разворачиваются из одного архива.*

После успешного развертывания клетки размещены в директории `/mnt`. Далее выполним старт этих клеток 
 
    sudo ezjail-admin start zebrahub zebraslave1 zebraslave2

и проверим список активных клеток

    sudo ezjail-admin list

Если все хорошо, то клетки установлены и работают синхронно с виртуальной машиной. Подготовка выполнена.


### Текущее обновление данных

Для *текущего* обновления данных необходимо придерживаться следующего порядка -

1. Обновление данных в `zebrahub`
2. Обновление данных в `zebraslave1` и в `zebraslave2` (порядок не имеет значения) 


Для обновления данных в`zebrahub` -

```
#!sh
sudo ezjail-admin console zebrahub
zebrahub#sh /usr/local/vhosts/cmd-update.sh
zebrahub#exit
```

Затем, по очереди или параллельно, обновляем данные в `zebraslave1` и `zebraslave2` -

```
#!sh
sudo ezjail-admin console zebraslave1
zebraslave1#sh /usr/local/vhosts/cmd-update.sh
zebraslave1#exit
```

```
#!sh
sudo ezjail-admin console zebraslave2
zebraslave2#sh /usr/local/vhosts/cmd-update.sh
zebraslave2#exit
```

### Текущее обновление данных по расписанию

Текущее обновление данных по расписанию выполняется на основе cron(1) в каждой из клеток по отдельности. Порядок обновления прежний -

1. Обновление `zebrahub`
2. Обновление `zebraslave1` и `zebraslave2`

Для создания записи в cron можно использовать файл `/usr/local/vhosts/crontab`. Этот файл есть в каждой из клеток и представляет собой пример использования cron(1) для обновления данных.

Типичный файл в формате crontab(5), выглядит так -

```
#!sh
SHELL=/bin/sh
MAILTO=root
*/15 * * * * $SHELL /usr/local/vhosts/cmd-update.sh
```

На основе этого файла система выполняет скрипт `/usr/local/share/cmd-update.sh` через каждые 15 минут.

### Полное обновление данных


Выполнять полное обновление данных можно с остановкой и без остановки поискового индекса. Рассмотрим последний вариант, так как на практике встречаются задачи, где необходимо обеспечивать непрерывную работу.

Полное обновление данных может выполняться так -

1. В `zebrahub` остановить сервис `zebrasrv`
2. В `zebrahub` удалить старые индексы и создать новые
3. В `zebrahub` запустить сервис `zebrasrv`

или то же самое, но на другом языке -

```
#!sh
zebrahub#service zebrasrv stop
zebrahub#sh /usr/local/vhosts/cmd-init.sh
zebrahub#env PERIOD_START=1970-01-01 sh /usr/local/vhosts/cmd-update.sh
zebrahub#service zebrasrv start
```

К этому моменту в `zebrahub` будут актуальные данные. На основе этих данных будем полностью обновлять данные в
`zebraslave1` и `zebraslave2`.

Для полного обновления данных в `zebraslave1` -

```
#!sh
zebraslave1#service gearman-worker-zebra.sh stop
zebraslave1#sh /usr/local/vhosts/cmd-init.sh
zebraslave1#env PERIOD_START=1970-01-01 sh /usr/local/vhosts/cmd-update.sh
zebraslave1#service gearman-worker-zebra.sh start
```

Далее выполняем команды по аналогии c `zebraslave1` для `zebraslave2` -

```
#!sh
zebraslave2#service gearman-worker-zebra.sh stop
zebraslave2#sh /usr/local/vhosts/cmd-init.sh
zebraslave2#PERIOD_START=1970-01-01 sh /usr/local/vhosts/cmd-update.sh
zebraslave2#service gearman-worker-zebra.sh start
```

В результате получаем полностью обновленные данные.

### Подключение воркеров

Для подключения поискового индекса к веб-приложению, которое работает на виртуальной машине 1, необходимо
указать в `zebraslave1` и `zebraslave2` адрес *сервера очередей* и затем выполнить рестарт *воркеров*
(`gearman-worker-zebra`).

Для этого выполним следующие действия в `zebraslave1` -

```
#!sh
zebraslave1#echo 'gearman_worker_zebra_flags="-H XXX.XXX.XXX.XX"' >> /etc/rc.conf
zebraslave1#service gearman-worker-zebra.sh restart
```
, где `XXX.XXX.XXX.XXX` - IP-адрес виртуальной машины 1, в которой работает сревер очередей.


По аналогии с `zebraslave1` выполняем действия в `zebraslave2` -

```
#!sh
zebraslave2#echo 'gearman_worker_zebra_flags="-H XXX.XXX.XXX.XX"' >> /etc/rc.conf
zebraslave2#service gearman-worker-zebra.sh restart
```

После успешного подключения *воркеров* к серверу очередей, запросы к поисковому индексу будут
обрабатываться в `zebraslave1` и `zebraslave2`.