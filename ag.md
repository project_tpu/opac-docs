**Руководство системного администратора**

* * *
[TOC]
* * *

# Введение

Программа OPAC распространяется в виде *образа виртуальной машины* в одном из форматов [OVA]() или [OVF](). Краткая
характеристика образа виртуальной машины представлена в Таблице 1.

*Таблица 1. Характеристика образа виртуальной машины*

Название             | Значение     
---------------------|-------------------
Формат               | OVA
Имя файла            | opac-*VERSION*.ova
ЦПУ                  | 2
ОЗУ                  | 2Гб
Жёсткий диск 1       | 31Гб
Жёсткий диск 2       | 104Гб
Операционная система | FreeBSD 11.2


Для создания виртуальной машины OPAC применяется вычислительная среда [Oracle VM VirtualBox]() версии 5.2.22.

# Развертывание

Развертывание программы OPAC выполняется в вычислительной среде. Список вычислительных сред, для которых выполнялось успешное развертывание программы OPAC, указан в разделе **Поддерживаемые вычислительные среды**.

Процесс развертывания программы OPAC в виртуальной среде состоит из 4 шагов

***Шаг 1. Импорт виртуальной машины***

Для импорта образа виртуальной машины в вычислительную среду используйте штатные средства для работы с архивом OVA. Импортируйте образ из файла `opac-VERSION.ova`, где VERSION - номер версии виртуальной машины.

При импорте образа виртуальной машины можно задать новые значения параметров создаваемой виртуальной машины. Например, можно задать нужное количество процессоров или изменить размер оперативной памяти. Последнее является важным для разумного распределения вычислительных ресурсов, и в случае если программа OPAC работает на нескольких виртуальных серверах. 

***Шаг 2. Запуск виртуальной машины***

Выполните запуск виртуальной машины штатными средствами виртуальной среды и в дальнейшем, пожалуйста, используйте штатные средства виртуальной среды для корректной работы с виртуальной машиной.

***Шаг 3. Получить контроль над виртуальной машиной***

Выполните первый вход. Для вода используете пару имя `libuser` и пароль `library`. При первом входе необходимо задать новый пароль для пользователя libuser с помощью команды passwd(1) -
 
```
#!sh
libuser@opac$ passwd
...
Old Password: ***
New Password: ***
Retype New Password: ***
```

***Шаг 4. Проверка развертывания***

*IP-адрес*

Убедитесь в правильной настройке сетевого интерфейса виртуальной машины. В VirtualBox сетевой адаптер виртуальной машины получает IP-адрес 10.0.2.15, который работает в режиме NAT (сетевая трансляции адресов). Для получения списка сетевых контроллеров с указанием  используйте команду ifconfig(1) -

```
#!sh
libuser@opac$ ifconfig -a
```

*Клетки*

Убедитесь в работе клеток (jails). Для получения информации о работающих клетках используйте команду ezjail-admin(8) из программного пакета ezjail. Для получения списка клеток введите команду в родительской системе -

```
#!sh
libuser@opac:~ % ezjail-admin list
```

Все клетки должны быть активны. 

*Порты*

Для проверки работы сервисов используйте команду sockstat(1).

```
#!sh
libuser@opac$ sockstat -4 -p 9998
libuser@opac$ sockstat -4 -p 4730
libuser@opac$ sockstat -4 -p 5432
libuser@opac$ sockstat -4 -p 3000
libuser@opac$ sockstat -4 -p 8000

```

# Администрирование

## Введение в управление клетками

Все основные компоненты программы OPAC работают в клетках (jails). Клетки работают независимо друг от друга и связаны между собой локальной сетью в родительской системе. За каждой из клеток закреплен отдельный IP-адрес. Благодаря этому мы можем войти в клетку по протоколу SSH, используя команду ssh(1) или обратиться к *сервису* по IP-адресу и порту, который работает в клетке. Список клеток и связанных сервисов приведен в Таблице 2.

*Таблица 2. Сервисы и клетки в программе OPAC*

Сервис                | IP-адрес   | Порт  | Клетка
----------------------|------------|-------|--------
zebrasrv              | 10.0.0.100 | 9998  | zebra         
gearmand              | 10.0.0.101 | 4730  | gearman
gearman-worker-zebra  | 10.0.0.101 | -     | gearman
postgresql            | 10.0.0.102 | 5432  | pg
gearman-worker-ruslan | 10.0.0.103 | -     | gearman-ruslan
nginx                 | 10.0.0.104 | 8000  | nginx
memcached             | 10.0.0.105 | 11211 | node
opac                  | 10.0.0.105 | 3000  | node


В штатном режиме клетки работают синхронно с родительской системой на основе сервиса ezjail, который запускает или останавливает клетки. Для непосредственного или ручного управления клетками применяется команда ezjail-admin(8). Непосредственное управление клетками применяется для различных задач, например для резервного копирования или простого управления сервисами. 

## Управление сервисами

### Простое управление сервисами

Для управления сервисами в форме простых действий (старт, стоп, рестарт) можно применять средства администрирования клеток на основе программы ezjail-admin(8). Если клетка содержит несколько сервисов, то очевидно, что действия на уровне клетки затронут все сервисы в клетке. На практике группирование сервисов в одной клетке обусловлено исключительно удобством использования и администрирования. Поэтому синхронные действия с сервисами в клетке мы находим удобным.  

Итак, если необходимо остановить работу сервиса, или наоборот запустить сервис в работу, то для этого определяем имя клетки (см. Таблицу 2), в которой расположен сервис, и далее выполняем необходимое действие. Например, если необходимо остановить работу приложения OPAC, которое работает в клетке node, то для этого подойдет команда

```
#!sh
libuser@opac$ sudo ezjail-admin stop node
```

Теперь клетка node остановлена и все сервисы в клетке также корректно остановлены. Для запуска приложения OPAC необходимо запустить клетку node в работу

```
#!sh
libuser@opac$ sudo ezjail-admin start node
```

Таким образом, можно достаточно просто управлять работой сервисов на уровне клеток.

В других случаях, когда нам нужно выполнить администрирование отдельного сервиса, мы предлагаем непосредственно войти в клетку по протоколу SSH и выполнить необходимые действия.    


### Управление сервисом - zebrasrv

Сервис `zebrasrv` работает в клетке node. Для выполнения задач администрирования требуется выполнить вход в клетку node по протоколу SSH

```
#!sh
libuser@opac@ssh libuser@10.0.0.100
```

и далее выполнить требуемые действия.

Действие                | Команда
------------------------|------------
Старт                   | `$sudo service zebrasrv start` 
Стоп                    | `$sudo service zebrasrv stop`
Рестарт                 | `$sudo service zebrasrv restart`
Получить статус         | `$sudo service zebrasrv status`

### Управление сервисом - gearmand

Сервис `gearmand` работает в клетке gearman. Для выполнения задач администрирования требуется выполнить вход в клетку node по протоколу SSH

```
#!sh
libuser@opac@ssh libuser@10.0.0.101
```

и далее выполнить требуемые действия.

Действие                           | Команда
-----------------------------------|------------
Старт                              | `$sudo service gearmand start` 
Стоп                               | `$sudo service gearmand stop`
Рестарт                            | `$sudo service gearmand restart`
Получить статус                    | `$sudo service gearmand status`


### Управление сервисом - gearman-worker-zebra

Сервис `gearman-worker-zebra` работает в клетке gearman. Для выполнения задач администрирования требуется выполнить вход в клетку node по протоколу SSH

```
#!sh
libuser@opac@ssh libuser@10.0.0.101
```

и далее выполнить требуемые действия.

Действие                           | Команда
-----------------------------------|------------
Старт                              | `$sudo service gearman-worker-zebra start` 
Стоп                               | `$sudo service gearman-worker-zebra stop`
Рестарт                            | `$sudo service gearman-worker-zebra restart`
Получить статус                    | `$sudo service gearman-worker-zebra status`


### Управление сервисом - gearman-worker-ruslan

Сервис `gearman-worker-ruslan` работает в клетке gearman-ruslan. Для выполнения задач администрирования требуется выполнить вход в клетку node по протоколу SSH

```
#!sh
libuser@opac@ssh libuser@10.0.0.103
```

и далее выполнить требуемые действия.

Действие                           | Команда
-----------------------------------|------------
Старт                              | `$sudo service gearmand-worker-ruslan start` 
Стоп                               | `$sudo service gearmand-worker-ruslan stop`
Рестарт                            | `$sudo service gearmand-worker-ruslan restart`
Получить статус                    | `$sudo service gearmand-worekr-ruslan status`


### Управление сервисом - postgresql

Сервис `postgresql` работает в клетке pg. Для выполнения задач администрирования требуется выполнить вход в клетку node по протоколу SSH

```
#!sh
libuser@opac@ssh libuser@10.0.0.102
```

и далее выполнить требуемые действия.

Действие               | Команда
-----------------------|------------
Старт                  | `sudo service posgresql start` 
Стоп                   | `sudo service posgresql stop`
Рестарт                | `sudo service posgresql restart`
Получить статус        | `sudo service posgresql status`



### Управление сервисом - memcached

Сервис `memcached` работает в клетке node. Для выполнения задач администрирования требуется выполнить вход в клетку node по протоколу SSH

```
#!sh
libuser@opac@ssh libuser@10.0.0.105
```

и далее выполнить требуемые действия.

Действие               | Команда
-----------------------|------------
Старт                  | `sudo service memcached start` 
Стоп                   | `sudo service memcached stop`
Рестарт                | `sudo service memcached restart`
Получить статус        | `sudo service memcached status`

### Управление сервисом - opac

Для получения текущего статуса работы веб-приложения OPAC можно использовать команду [pm2](). Для этого нужно сначала войти в клетку node и затем выполнить команду pm2.

```
#!sh
libuser@opac$ ssh libuser@10.0.0.105
Password for libuser@node: ***
libuser@node:~ % pm2 status
┌──────────┬────┬──────────┬─────────┬───────┬────────┬─────────┬────────┬─────┬───────────┬─────────┬──────────┐
│ App name │ id │ version  │ mode    │ pid   │ status │ restart │ uptime │ cpu │ mem       │ user    │ watching │
├──────────┼────┼──────────┼─────────┼───────┼────────┼─────────┼────────┼─────┼───────────┼─────────┼──────────┤
│ opac     │ 0  │ 0.0.2RC2 │ cluster │ 45647 │ online │ 29      │ 2D     │ 0%  │ 55.9 MB   │ libuser │ disabled │
│ opac     │ 1  │ 0.0.2RC2 │ cluster │ 45648 │ online │ 29      │ 2D     │ 0%  │ 55.4 MB   │ libuser │ disabled │
└──────────┴────┴──────────┴─────────┴───────┴────────┴─────────┴────────┴─────┴───────────┴─────────┴──────────┘
```

Команда pm2 имеет развитый набор команд и позволяет получить полное представление о работе приложения, в том
числе выполнять мониторинг работы приложения в онлайн режиме.

### Управление сервисом - nginx

Сервис `nginx` работает в клетке nginx. Для выполнения задач администрирования требуется выполнить вход в клетку node по протоколу SSH

```
#!sh
libuser@opac@ssh libuser@10.0.0.104
```

и далее выполнить требуемые действия.

Действие               | Команда
-----------------------|------------
Старт                  | `sudo service nginx start` 
Стоп                   | `sudo service nginx stop`
Рестарт                | `sudo service nginx restart`
Получить статус        | `sudo service nginx status`

## Обновление данных

### Создание поискового индекса

Сразу после развертывания поисковый индекс работает, но не содержит данных. Эти данные нудно загрузить из
одного или нескольких источников. Далее необходимо создать поисковый индекс. 

```
#!sh
libuser@zebra$ cd /usr/local/vhosts
libuser@zebra$ sudo make vhosts
libuser@zebra@ sudo service zebrasrv restart
libuser@zebra$ sudo make init fetch update commit
```
Создание поискового индекса может занять длительное время.

### Текущее обновление поискового индекса OPAC

Текущее обновление поискового индекса это процесс синхронизации локальных базы данных с внешними источниками данных, которое может выполняться в двух режимах -

* Интерактивный режим

Для интерактивного обновления поискового индекса OPAC необходимо выполнить вход в клетку zebra и последовательно выполнить следующие команды

```
#!sh
libuser@zebra$ cd /usr/local/vhosts
libuser@zebra$ make fetch update commit
```

* Автоматический режим

Для автоматического обновления поискового индекса OPAC необходимо в клетке zebra выполнять запуск скрипта

```
/usr/local/etc/idzebra-contrib-vhosts/periodic/dayly/100.sync
```

по расписанию на основе cron(8). Скрипт и подготовленный crontab находятся в клетке zebra.

### Обновление поискового индекса

> Полное обновление поискового индекса может занять длительное время (более 4 часов для 500 000 записей).

Полное обновление поискового индекса OPAC подразумевает удаление старого индекса и создание нового. В течение этого процесса поисковый индекс не будет доступен для работы. Процесс полного обновления индекса состоит из 5 шагов.

***Шаг 1. Войти в клетку zebra***
```
#!sh
libuser@opac$ ssh libuser@10.0.0.100
libuser@zebra$
```

***Шаг 2. Запретить текущее автоматическое обновление индекса***

Используя команду crontab(1) внесите необходимые изменения в corontab пользователя libuser


***Шаг 3. Удалить существующий индекс***

```
#!sh
libuser@zebra$ cd /usr/local/vhost
libuser@zebra$ make clean
```

***Шаг 4. Создать новый индекс***

```
#!sh
libuser@zebra$ cd /usr/local/vhosts
libuser@zebra$ make init
libuser@zebra$ make fetch update commit
libuser@zebra$ make fetch update commit
```
***Шаг 5. Разрешить текущее автоматическое обновление индекса***

Разрешить текущее обновление индекса на основе команды crontab(1).

## Резервное копирование

Резервное копирование OPAC можно выполнять либо на уровне виртуальной машины OPAC, либо на уровне клеток. А также, можно комбинировать оба этих подхода. Далее мы рассмотрим резервное копирование на уровне клеток, так как этот подход не зависит от особенностей вычислительной среды.

Резервное копирование клеток основано на управлении архивными файлами клеток с помощью ezjail-admin(8). Для каждой клетки создается архивный файл, из которого можно в последствии восстановить клетку.

 Клетка | Резервное копирование | Данные
--------|-----------------------|--------------------------
zebra   | Опциональное          | Конф. файлы, лог-файлы 
gearman | Опциональное          | Конф. файлы, лог-файлы
gaerman-ruslan | Опциональное   | Конф. файлы, лог-файлы
pg      | Обязательное          | Уникальные данные пользователей
nginx   | Опциональное          | Конф. файлы
node    | Опциональное          | Конф. файлы


Рассмотрим пример создания архива pg.backup для клетки pg, а также восстановление клетки pg из архива pb.backup.
Этот пример подойдет к любой другой клетке.

```
#!sh
libuser@opac$ sudo ezjail-admin stop pg
libuser@opac$ sudo ezjail-admin archive -a pg.backup pg
```

В результате этих действий будет создан архив pg.backup.tar.gz, который хранится в `/usr/jails/ezhjail_archives`.

Для восстановления клетки pg из резервной копии необходимо установить клетку pg из архива pg.backup. Для этого необходимо выполнить следующее

```
#!sh
root@opac#ezjail-admin stop pg
root@opac#ezjail-admin delete -w pg
root@opac#ezjail-admin restore pg.backup.tar.gz
root@opac#ezjail-admin start pg
```


# Различное

## Назначение jail-ов OPAC

Клетка  | IP:PORT         | Сервер 
--------|-----------------|-----------
zebra   | 10.0.0.100:9998 | Zebra
gearman | 10.0.0.101:4730 | Gearmand, gearman-worker-zebra
pg      | 10.0.0.102:5432 | PostgreSQL
gearman-ruslan | 10.0.0.103:8000 | gearman-woker-ruslan
nginx   | 10.0.0.104:8000 | nginx
node    | 10.0.0.105:3000 | Node.js

## Список баз данных OPAC

Ресурс     | URL
-----------|-----------------------------
catalogue  | http://catalogue.vh.lib.tpu.ru:9998/catalogue
serials2   | http://serials2.vh.lib.tpu.ru:9998/serials2
names      | http://names.vh.lib.tpu.ru:9998/names
orgs       | http://orgs.vh.lib.tpu.ru:9998/orgs
subj       | http://subj.vh.lib.tpu.ru:9998/subj
ec         | http://ec.vh.lib.tpu.ru:9998/ec
ap         | http://ap.vh.lib.tpu.ru:9998/ap


## Поддерживаемые вычислительные среды

Виртуальная машина OPAC может работать в следующих вычислительных средах

1. Oracle VM VirtualBox 5.0.14
2. VMware Workstation 12 Player